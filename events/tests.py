from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.utils import timezone
from datetime import datetime
from .models import EventEntry
from .models import Attendant
from .views import events

# Create your tests here.
class EventsUnitTest(TestCase):

    def test_events_url_is_exist(self):
        response = Client().get('/events/')
        self.assertEqual(response.status_code,200)

    def test_events_using_events_template(self):
        response = Client().get('/events/')
        self.assertTemplateUsed(response, 'events.html')

    def test_events_using_events_func(self):
        found = resolve('/events/')
        self.assertEqual(found.func, events)

    def test_model_can_create_attendants(self):
        new_attendant = Attendant.objects.create(username='TestName',
            password='TestPassword', email='testemail@ui.ac.id')
        counting_all_available_attendants = Attendant.objects.all().count()
        self.assertEqual(counting_all_available_attendants, 1)
		
    def test_model_can_create_events(self):
        # Creating a new event
        new_event = EventEntry.objects.create(
            title='Test',
            description='Test',
            date=timezone.now(),
            time=datetime.now(),
            place='Test',
        )
    
        # Retrieving all available events
        counting_all_available_events = EventEntry.objects.all().count()
        self.assertEqual(counting_all_available_events, 1)

    def test_events_html_contains_title(self):
        response = Client().get('/events/')
        response_content = response.content.decode('UTF-8')
        self.assertIn('Events', response_content)