from django.db import models

# Create your models here.

class Attendant(models.Model):
    username = models.CharField(max_length = 50)
    password = models.CharField(max_length = 50)
    email = models.EmailField()
	
class EventEntry(models.Model):
    title = models.CharField(max_length = 255)
    date = models.DateField()
    time = models.TimeField()
    place = models.CharField(max_length = 255)
    description = models.CharField(max_length = 255)
    attendant = models.ManyToManyField(Attendant, blank=True)