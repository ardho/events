# Generated by Django 2.1.1 on 2018-10-18 05:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0002_attendant'),
    ]

    operations = [
        migrations.AddField(
            model_name='evententry',
            name='attendant',
            field=models.ManyToManyField(to='events.Attendant'),
        ),
    ]
