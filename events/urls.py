from django.urls import path
from . import views

app_name = 'events'

urlpatterns = [
    path('', views.events),
    path('<reg_event>', views.register)
]
