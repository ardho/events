from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.utils import timezone
from datetime import datetime
from .views import about
from .models import Testimoni
from .forms import FormTestimoni

class AboutUnitTest(TestCase):

    def test_about_url_is_exist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code,200)

    # def test_can_call_addtestimoni(self):
    #     response = Client().get('/about/tambah/')
    #     self.assertEqual(response.status_code,200)

    def test_RegMember_using_About_template(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'about.html')

    # def test_RegMember_using_RegMember_func(self):
    #     found = resolve('/register-as-member/')
    #     self.assertEqual(found.func, memregister)

    def test_index_contains_about(self):
        response = Client().get('/about/')
        response_content = response.content.decode('UTF-8')
        self.assertIn('About', response_content)

    def test_model_can_create_new_testimoni(self):
        Testimoni.objects.create(nama='Anon', username='abc', testi="isi testi", waktu=timezone.now())
        counting_all_available_user = Testimoni.objects.all().count()
        self.assertEqual(counting_all_available_user, 1)

    def test_form_validation_for_blank_items(self):
        form = FormTestimoni(data={'testi':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['testi'],
            ['This field is required.']
        )
