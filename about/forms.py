from django import forms
from django.forms import ModelForm

from .models import Testimoni

class FormTestimoni(ModelForm):
    class Meta:
        model = Testimoni
        fields = ['testi']
        testi = forms.CharField(widget=forms.Textarea, label="")
        widgets = {
            'testi': forms.Textarea(attrs={'placeholder': "Berikan testimoni Anda!"})
        }

        #get username dan nama dari google api, waktu auto set
