from django.db import models
from django import forms

class Testimoni(models.Model):
    nama = models.CharField(max_length=30)
    username = models.CharField(max_length=30)
    testi = models.CharField(max_length=200)
    waktu = models.DateField(auto_now_add=True, blank=True)

    def __str__(self):
        return self.username
