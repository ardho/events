from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.utils import timezone
from datetime import datetime
from .views import memregister
from .models import User
from .forms import RegisterForm

# Create your tests here.
class RegMemberUnitTest(TestCase):

    def test_RegMember_url_is_exist(self):
        response = Client().get('/register-as-member/')
        self.assertEqual(response.status_code,200)

    def test_RegMember_using_RegMember_template(self):
        response = Client().get('/register-as-member/')
        self.assertTemplateUsed(response, 'register-member.html')

    def test_RegMember_using_RegMember_func(self):
        found = resolve('/register-as-member/')
        self.assertEqual(found.func, memregister)

    def test_index_contains_greeting(self):
        response = Client().get('/register-as-member/')
        response_content = response.content.decode('UTF-8')
        self.assertIn('Daftar sebagai member', response_content)

    def test_model_can_create_new_user(self):
        User.objects.create(nama='Anon', username='abc', email='abc@email.com', password='abcde123', tgllhr=timezone.now(), alamat='Jalan asal')
        counting_all_available_user = User.objects.all().count()
        self.assertEqual(counting_all_available_user, 1)

    def test_form_validation_for_blank_items(self):
        form = RegisterForm(data={'nama':'', 'email':'', 'password':'', 'username':'', 'tgllhr':'', 'alamat':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['nama'],
            ['This field is required.']
        )