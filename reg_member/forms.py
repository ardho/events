from django import forms
from django.forms import ModelForm

from .models import User

class DateInput(forms.DateInput):
    input_type = 'date'

class RegisterForm(ModelForm):
    class Meta:
        model = User
        password = forms.CharField(widget=forms.PasswordInput)
        fields = ['nama', 'username', 'email', 'tgllhr', 'password', 'alamat']
        widgets = {
            'waktu': DateInput(),
            'password': forms.PasswordInput(),
        }


        # pull dari git kalo rusak lmao
