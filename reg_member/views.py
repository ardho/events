from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView
from django.http import HttpResponseRedirect
from django import template

from .forms import RegisterForm
from .models import User

def memregister(request):

    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            temp = form.save()

    else:
        form = RegisterForm()

    context = {'form': form,}

    return render(request, 'register-member.html', context)
