from django.db import models
from django import forms
from django.utils.safestring import mark_safe

# Create your models here.
class User(models.Model):
        nama = models.CharField(max_length = 30)
        username = models.CharField(unique = True, max_length = 30)
        email = models.EmailField(unique = True,)
        tgllhr = models.DateField()
        password = models.CharField(max_length = 50)
        alamat = models.CharField(max_length = 30)

        def __str__(self):
            return self.username
