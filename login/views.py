from django.shortcuts import render
from django.contrib.auth import logout
from django.http import HttpResponseRedirect


# Create your views here.
def signin(request):
    return render(request, '')

def signout(request):
    logout(request)
    return HttpResponseRedirect('/events')

def verifikasi(request):
    return render(request, 'google17b56790938cb27b.html')
