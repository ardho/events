from django.contrib import admin
from .models import GoogleUser

# Register your models here.
admin.site.register(GoogleUser)
