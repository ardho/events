from django.db import models
from django import forms
from django.utils.safestring import mark_safe
from events.models import EventEntry

EVENT_LIST = []
for event in EventEntry.objects.all():
	EVENT_LIST.append((event.title, event.title))

# Create your models here.
class Participant(models.Model):
	username = models.CharField(unique = True, max_length = 30)
	email = models.EmailField(unique = True)
	password = models.CharField(max_length = 50)
	event_list = models.CharField(choices = EVENT_LIST, max_length = 255)

	def __str__(self):
		return self.username