from django.test import TestCase, Client
from django.urls import resolve
from .models import Participant
from .views import indexRegParticipantForm
from .forms import RegParticipantForm
from events.models import EventEntry, Attendant

# Create your tests here.
class RegParticipantUnitTest(TestCase):

	def test_RegParticipant_url_is_exist(self):
		response = Client().get('/register-as-participant/')
		self.assertEqual(response.status_code, 200)

	def test_using_view_func(self):
		found = resolve('/register-as-participant/')
		self.assertEqual(found.func, indexRegParticipantForm)

	def test_using_index_template(self):
		response = Client().get('/register-as-participant/')
		self.assertTemplateUsed(response, 'register-participant.html')

	def test_index_contains_greeting(self):
		response = Client().get('/register-as-participant/')
		response_content = response.content.decode('UTF-8')
		self.assertIn('Daftar ikut event', response_content)

	def test_model_can_create_new_participant(self):
		EventEntry.objects.create(title='test', date='2018-10-10', time='12:00')
		Participant.objects.create(username='abc', email='abc@email.com', password='abcde123', event_list='test')
		counting_all_available_participant = Participant.objects.all().count()
		self.assertEqual(counting_all_available_participant, 1)

	def test_form_validation_for_blank_items(self):
		form = RegParticipantForm(data={'username':'', 'email':'', 'password':'', 'event_list':''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
		    form.errors['username'],
		    ['This field is required.']
		)

	def test_RegParticipant_post_success_and_render_the_result_in_event(self):
		EventEntry.objects.create(title='test', date='2018-10-10', time='12:00')
		Attendant.objects.create(username='abc')
		EventEntry.objects.all().get(title = 'test').attendant.add(Attendant.objects.all().get(username='abc'))
		response_post = Client().post('/register-as-participant/', username='abc', event_list='test')
		self.assertEqual(response_post.status_code, 200)

		response = Client().get('/events/')
		html_response = response.content.decode('UTF-8')
		self.assertIn('abc', html_response)

	def test_unique_username(self):
		Participant.objects.create(username='a', email='abc123@abc123.com', password='oo', event_list='test')
		form = RegParticipantForm(data={'username':'a', 'email':'def123@def123.com', 'password':'qw', 'event_list':'test'})
		self.assertFalse(form.is_valid())
