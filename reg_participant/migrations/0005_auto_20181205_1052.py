# Generated by Django 2.1.1 on 2018-12-05 03:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reg_participant', '0004_auto_20181020_1623'),
    ]

    operations = [
        migrations.AlterField(
            model_name='participant',
            name='event_list',
            field=models.CharField(choices=[('UTS Start Day', 'UTS Start Day'), ('CompFest X Main Event', 'CompFest X Main Event'), ('Rutinitas Malam Kamisan Edisi Spesial', 'Rutinitas Malam Kamisan Edisi Spesial'), ('Muskan', 'Muskan'), ('Bernapas Berjamaah', 'Bernapas Berjamaah'), ('ACM ICPC Regional Jakarta', 'ACM ICPC Regional Jakarta'), ('TP1 Pepew', 'TP1 Pepew')], max_length=255),
        ),
    ]
