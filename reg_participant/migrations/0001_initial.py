# Generated by Django 2.1.1 on 2018-10-18 07:52

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Participant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=15)),
                ('email', models.CharField(max_length=20)),
                ('password', models.CharField(default='', max_length=50)),
                ('event_list', models.CharField(choices=[('CFX', 'CFX'), ('Muskan', 'Muskan'), ('CGT', 'CGT'), ('Betis', 'Betis')], max_length=10)),
            ],
        ),
    ]
