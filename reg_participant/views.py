from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import TemplateView
from django import template

from .forms import RegParticipantForm
from .models import Participant
from events.models import EventEntry, Attendant

def indexRegParticipantForm(request):
	if request.method == 'POST':
		form = RegParticipantForm(request.POST)
		if form.is_valid():
			temp = form.save()

			participant = Attendant(username = temp.username, password = temp.password, email = temp.email)
			participant.save()
			EventEntry.objects.all().get(title = temp.event_list).attendant.add(participant)

			return redirect('../events/')
	else:
		form = RegParticipantForm()

	response = {
		'form': form
	}
	return render(request, 'register-participant.html', response)

