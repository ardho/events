from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import news
from django.utils import timezone
from datetime import datetime
from .models import News
from .models import User

# Create your tests here.
class NewsUnitTest(TestCase):

    def test_news_url_is_exist(self):
        response = Client().get('/news/')
        self.assertEqual(response.status_code,200)

    def test_news_using_news_template(self):
        response = Client().get('/news/')
        self.assertTemplateUsed(response, 'news.html')

    def test_news_using_news_func(self):
        found = resolve('/news/')
        self.assertEqual(found.func, news)

    def test_model_can_create_news(self):
        # Creating a new news
        new_news = News.objects.create(
            title='Test',
            description='Test',
            date=timezone.now(),
            code="Test"
        )
    
        # Retrieving all available news
        counting_all_available_news = News.objects.all().count()
        self.assertEqual(counting_all_available_news, 1)

    def test_model_can_create_user(self):
        new_user = User.objects.create(username='usertest', news_code='aaaaa')
        counting_all_available_user = User.objects.all().count()
        self.assertEqual(counting_all_available_user, 1)

    def test_news_html_contains_title(self):
        response = Client().get('/events/')
        response_content = response.content.decode('UTF-8')
        self.assertIn('News', response_content)
