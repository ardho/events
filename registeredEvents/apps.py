from django.apps import AppConfig


class RegisteredeventsConfig(AppConfig):
    name = 'registeredEvents'
